<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\CreatePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Break_;

use function PHPUnit\Framework\callback;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware(['validateAuthor'])->only('edit', 'update', 'destroy', 'trash');
        $this->middleware(['verifyCategoriesCount'])->only('create', 'store');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        if(auth()->user()->isAdmin())
        {
            $posts = Post::approve()->paginate(10);
        }else{
            $posts = Post::approve()->where('user_id', auth()->id())->paginate(10);
        }

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.create', compact(['categories','tags']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        //

        $image = $request->file('image')->store('images/posts');
        $post = Post::create([
            'title' => $request->title,
            'excerpt' => $request->excerpt,
            'content' => $request->content,
            'category_id' => $request->category_id,
            'user_id' => auth()->id(),
            'image' => $image,
            'published_at' => $request->published_at,

        ]);

        $post->tags()->attach($request->tags);
        session()->flash('success','Post created successfully and has been went for admin approval!');
        return redirect(route('posts.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.edit', compact(['post','categories', 'tags']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $data = $request->only(['title','excerpt','content','published_at','category_id']);
        if($request->hasFile('image')){
            $image = $request->image->store('images/posts');
            $data['image'] = $image;
            $post->deleteImage();
        }
        $post->update($data);

        $post->tags()->sync($request->tags);
        session()->flash('success', 'Post updatd successfully!');
        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->deleteImage();
        $post->forceDelete();
        session()->flash('success','Post deleted successfully');
        return redirect(route('posts.trashed'));
    }

    public function trashed()
    {
        $trashed = Post::onlyTrashed()->paginate(10);
        return view('posts.trashed',['posts' => $trashed]);
    }

    public function trash(Post $post)
    {
        $post->delete();
        session()->flash('success', 'Post trashed');
        return redirect(route('posts.index'));
    }
    public function draft(Post $post)
    {
        $post->update(['published_at' => NULL]);
        session()->flash('success', 'Post drafted successfully!');
        return redirect(route('posts.index'));
    }
    public function restore($id)
    {
        $trashedPost = Post::onlyTrashed()->findOrFail($id);
        $trashedPost->restore();
        session()->flash('success', 'Post restored successfully!');
        return redirect((route('posts.index')));
    }

    public function drafts()
    {
        $drafts = Post::drafted()->paginate(3);
        return view('posts.drafted', ['posts' => $drafts]);
    }

    public function publishDraft(Post $post)
    {
        $post->update(['published_at' => now()]);

        session()->flash('success', 'Post published successfully!');
        return redirect(route('posts.index'));
    }

    public function approved(Request $request, Post $post)
    {
        $post->update(['approved_at' => now()]);
        $post->update(['disapprove_reason' => NULL]);
        session()->flash('success', 'Post approved successfully!');
        return redirect(route('posts.index'));
    }

    public function approval()
    {
            $posts = Post::oldest('published_at')->published()->paginate(10);
            return view('posts.approval', compact('posts'));

    }

    public function disapproved(Request $request,Post $post)
    {
        $post->update(['disapprove_reason' => $request->exampleRadios]);
        $post->update(['approved_at' => NULL]);
        session()->flash('error', "Post has been disapproved! reason: $request->exampleRadios");
        return redirect(route('posts.index'));
    }

    public function disapprove_reason(Post $post)
    {
        $post = Post::findOrFail($post->id);
        return view('posts.disappove',compact('post'));
    }

}
