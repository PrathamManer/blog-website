<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;
    const APPROVED = NULL;
    protected $guarded = ['id'];

    protected $dates = ['published_at'];

    public function getImagePathAttribute()
    {
        return 'storage/'.$this->image;
    }

    public function getLikesCountAttribute()
    {
        return $this->likes->count();
    }
    public function getIsLikedAttribute()
    {
        return $this->likes()->where('user_id',auth()->id())->count() > 0;
    }

    public function getLikesStyleAttribute()
    {
        if($this->getIsLikedAttribute())
        {
            return 'color-red';
        }
        return 'color-grey';
    }
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    public function hasTag(int $tag_id): bool
    {
        return in_array($tag_id,$this->tags->pluck('id')->toArray());
    }

    public function deleteImage()
    {
        Storage::delete($this->image);
    }

    /*
    Query Scopes
    */
    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', now());
    }

    public function scopeDrafted($query)
    {
        return $query->where('published_at', '>', now())->orWhere('published_at', '=', NULL);
    }

    public function scopeSearch($query)
    {
        $search = request('search');
        if($search)
        {
            return $query->where('title', 'like', "%$search%");
        }

        return $query;
    }

    public function scopeApprove($query)
    {
        return $query->where('approved_at', '<=', now())->where('published_at', '<=', now());
    }

    public function isApproved() :bool
    {
        return $this->approved_at !== self::APPROVED;
    }

    public function likes()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
