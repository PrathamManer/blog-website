<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\LikesController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('blogs.home');
Route::get('/blogs/{post}', [FrontendController::class,'show'])->name('blogs.show');
Route::get('blogs/category/{category}', [FrontendController::class, 'category'])->name('blogs.category');
Route::get('blogs/tags/{tag}', [FrontendController::class, 'tag'])->name('blogs.tag');
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
Route::get('/disapprove/{post}/reason', [PostsController::class, 'disapprove_reason'])->name('posts.reason');




require __DIR__.'/auth.php';

// Application routes
// NOTE: have used raw url instead of categories.destroy

Route::middleware(['auth'])->group(function(){
    Route::resource('categories', CategoriesController::class);
    Route::resource('tags', TagsController::class);
    Route::put('/posts/approved/{post}', [PostsController::class, 'approved'])->name('posts.approved');
    Route::put('/posts/disapproved/{post}', [PostsController::class, 'disapproved'])->name('posts.disapproved');
    Route::put('posts/draft/{post}', [PostsController::class, 'draft'])->name('posts.draft');
    Route::get('posts/drafts', [PostsController::class, 'drafts'])->name('posts.drafts');
    Route::put('posts/drafts/{post}/publish', [PostsController::class, 'publishDraft'])->name('posts.publish-draft');
    Route::get('/posts/pending', [PostsController::class, 'approval'])->name('posts.approval')->middleware('admin');

    Route::put('posts/restore/{post}', [PostsController::class, 'restore'])->name('posts.restore');
    Route::delete('posts/trash/{post}', [PostsController::class, 'trash'])->name('posts.trash');
    Route::get('posts/trashed', [PostsController::class, 'trashed'])->name('posts.trashed');
    Route::post('posts/{post}/comments', [CommentController::class, 'store'])->name('posts.comment');
    Route::get('posts/comments', [CommentController::class, 'comment'])->name('posts.allComments')->middleware('admin');
    Route::resource('posts', PostsController::class);
    Route::put('/posts/approve/{comment}', [CommentController::class, 'approveComment'])->name('comment.approve-comment');
    Route::put('/posts/disapprove/{comment}', [CommentController::class, 'disapproveComment'])->name('comment.disapprove-comment');
    Route::get('/posts/{comment}/disapprove/reason', [CommentController::class, 'disapproveReason'])->name('comment.disapprove-reason');

    Route::post('posts/{post}/like', [LikesController::class,'store'])->name('posts.like');
    Route::delete('posts/{post}/dislike', [LikesController::class,'destroy'])->name('posts.unlike');

});

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/users', [UsersController::class, 'index'])->name('users.index');
    Route::put('/users/{user}/make-admin', [UsersController::class, 'makeAdmin'])->name('users.make-admin');
    Route::put('/users/{user}/revoke-admin', [UsersController::class, 'revokeAdmin'])->name('users.revoke-admin');
});


