@extends('layouts.admin-panel.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Why are you Disapproving?</h2>
        </div>
        <div class="card-body">

           <form action="{{route('posts.disapproved',$post->id)}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-check">
                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="Inappropriate Content">
                <label class="form-check-label" for="exampleRadios1">
                    Inappropriate Content
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="Links are from or lead to harmful or inappropriate sites">
                <label class="form-check-label" for="exampleRadios2">
                    Links are from or lead to harmful or inappropriate sites
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="False Information">
                <label class="form-check-label" for="exampleRadios1">
                    False Information
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios4" value="Hate speech or Symbols">
                <label class="form-check-label" for="exampleRadios2">
                    Hate speech or Symbols
                </label>
            </div>
            <button href="" type='submit' class="btn btn-sm btn-outline-danger">Submit</button>
        </form>
        </div>
    </div>
@endsection
