
@extends('layouts.admin-panel.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h2>Manage Blogs</h2>
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th scope="col">Excerpt</th>
                    <th scope="col">Category</th>
                    <th scope="col">Actions</th>
                    <th scope="col">Reason</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($posts as $post)
                <tr>
                    <td><img src="{{ $post->image_path }}" width="120"></td>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->excerpt }}</td>
                    <td>{{ $post->category->name }}</td>

                    <td>
                       @if (! $post->isApproved())
                            <form action="{{route('posts.approved',$post->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <button href="" type='submit' class="btn btn-sm btn-outline-success">Approve Post</button>
                            </form>
                       @else
                            <form action="{{route('posts.reason',$post->id)}}" method="POST">
                                @csrf
                                @method('GET')
                                <button href="" type='submit' class="btn btn-sm btn-outline-danger">Disapprove Post</button>
                            </form>
                       @endif
                        </td>
                    <td>{{  $post->disapprove_reason  }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
<div class="mt-5">
    {{ $posts->links('vendor.pagination.bootstrap-4') }}
</div>
@endsection
