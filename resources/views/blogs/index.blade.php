@extends('layouts.frontend.layout')
@section('title', 'Pen-It')
@section('header')
    <header class="pt100 pb100 parallax-window-2" data-parallax="scroll" data-speed="0.5"
        data-image-src="{{ asset('frontend/assets/img/bg/img-bg-17.jpg') }}" data-positiony="1000">
        <div class="intro-body text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 pt50">
                        <h1 class="brand-heading font-montserrat text-uppercase color-light" data-in-effect="fadeInDown">
                            Pen-It
                            <small class="color-light alpha7">Heaven for Bloggers!</small>
                        </h1>
                    </div>
                </div>
            </div>

        </div>
    </header>
@endsection

@section('main-content')
<style>
    .display-inline{
        display: inline;
    }
    .bg-white{
        background-color: white;
    }

    .likes-count{
        margin-right: 92px;
        margin-top: -9px;
    }
</style>
    <div class="row">
        @foreach ($posts as $post)
            <div class="col-md-4 col-sm-6 col-xs-12 mb50">
                <h4 class="blog-title"><a href="{{route('blogs.show', $post->id) }}">{{ $post->title }}</a></h4>
                <div class="blog-three-attrib">
                    <span class="icon-calendar"></span>{{$post->published_at->diffForHumans()}}|
                    <span class=" icon-pencil"></span><a href="#">{{ $post->author->name }}</a>
                </div>
                <img src={{ asset("$post->image_path") }} class="img-responsive" alt="image blog">
                <p class="mt25">
                    {{ $post->excerpt }}
                </p>
                @if ($post->views_count === 1)
                    <p> {{$post->views_count}} View</p>
                @else
                    <p> {{$post->views_count}} Views</p>
                @endif

                <div class="row">
                   <div class="col-md-12">

                            <a href="{{route('blogs.show', $post->id)}}" class="button button-gray button-xs mr10">Read More <i class="fa fa-long-arrow-right"></i></a>

                            <form action="{{  route($post->is_liked ? 'posts.unlike' : 'posts.like', $post->id)}}" method="POST" class="display-inline">
                                @csrf
                                    @if ($post->is_liked)
                                        @method('DELETE')
                                    @endif
                                    <button type="submit" class="btn bg-white {{ $post->likes_style }}">
                                        <i class="fa fa-heart fa-2x"></i>
                                    </button>
                                    <h6 class="text-right likes-count">{{ $post->likes_count }}</h6>
                            </form>


                   </div>
                </div>



            </div>
        @endforeach
    </div>
    {{ $posts->appends(['search' => request('search')])->links('vendor.pagination.simple-default') }}

@endsection
